<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Grocery List</title>
</head>
<body>
<h1><%= "Welcome to Tanner's Grocery List" %>
</h1>
<br/>
<a href="${pageContext.request.contextPath}/list.html">Click here to continue to the Grocery List!</a>
</body>
</html>