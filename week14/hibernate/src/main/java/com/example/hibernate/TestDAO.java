package com.example.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;

public class TestDAO {
    SessionFactory factory = null;
    Session session = null;
    private static TestDAO single_instance = null;
    private TestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }
    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }
        return single_instance;
    }
    public List<GroceryList> getItems() {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.hibernate.GroceryList";
            List<GroceryList> cs = (List<GroceryList>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }
    public GroceryList getItems(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from com.example.hibernate.GroceryList where id=" + Integer.toString(id);
            GroceryList c = (GroceryList)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}