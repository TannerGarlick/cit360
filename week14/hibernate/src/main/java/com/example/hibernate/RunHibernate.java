package com.example.hibernate;

import com.example.hibernate.TestDAO;
import java.util.*;

public class RunHibernate {
    public static void main(String[] args) {
        TestDAO t = TestDAO.getInstance();
        List<GroceryList> c = t.getItems();
        for (GroceryList i : c) {
            System.out.println(i);
        }
        System.out.println(t.getItems(1));
    }
}