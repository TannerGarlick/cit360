public class ListItems {
    private String item1;
    private String item2;
    private String item3;
    private String item4;
    private String item5;
    private String item6;

    public String getName1() {
        return item1;
    }
    public void setName1(String name) {
        this.item1 = name;
    }
    public String getName2() {
        return item2;
    }
    public void setName2(String name) {
        this.item2 = name;
    }
    public String getName3() {
        return item3;
    }
    public void setName3(String name) {
        this.item3 = name;
    }
    public String getName4() {
        return item4;
    }
    public void setName4(String name) {
        this.item4 = name;
    }
    public String getName5() {
        return item5;
    }
    public void setName5(String name) {
        this.item5 = name;
    }
    public String getName6() {
        return item6;
    }
    public void setName6(String name) {
        this.item6 = name;
    }
    public String toString() {
        return " 1. "  + item1 + " |" + " 2. " + item2 + " |" + " 3. " + item3 + " |" + " 4. " + item4 + " |" + " 5. " + item5 + " |" + " 6. " + item6;
    }
}
