import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
public class Json {
    public static String ListItemsToJSON(ListItems list) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(list);
        } catch (JsonProcessingException e) {
            System.out.println("An error has occurred!");
            System.err.println(e.toString());
        }
        return s;
    }
    public static ListItems JSONToListItems(String s) {

        ObjectMapper mapper = new ObjectMapper();
        ListItems list = null;
        try {
            list = mapper.readValue(s, ListItems.class);
        } catch (JsonProcessingException e) {
            System.out.println("An error has occurred!");
            System.err.println(e.toString());
        }
        return list;
    }
    public static void main(String[] args) {
        ListItems lis = new ListItems();
        lis.setName1("Soda");
        lis.setName2("Cereal");
        lis.setName3("Frozen dinners");
        lis.setName4("Salty snacks");
        lis.setName5("milk");
        lis.setName6("bread");


        String json = ListItemsToJSON(lis);
        System.out.println(json);

        ListItems lis2 = JSONToListItems(json);
        System.out.println("Here are some of the most popular things to add to your grocery list!");
        System.out.println(lis2);
    }
}