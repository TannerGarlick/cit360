import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
public class json {
    public static String BusinessToJSON(Business business) {
        ObjectMapper mapper = new ObjectMapper();
        String s = "";
        try {
            s = mapper.writeValueAsString(business);
        } catch (JsonProcessingException e) {
            System.out.println("An error has occurred!");
            System.err.println(e.toString());
        }
        return s;
        }
        public static Business JSONToBusiness(String s) {

            ObjectMapper mapper = new ObjectMapper();
            Business business = null;
            try {
                business = mapper.readValue(s, Business.class);
            } catch (JsonProcessingException e) {
                System.out.println("An error has occurred!");
                System.err.println(e.toString());
            }
            return business;
        }
        public static void main(String[] args) {
            Business bus = new Business();
            bus.setName("Tanner");
            bus.setPhone(775633322);
            bus.setAddress("3242 Sheepskin dr");
            bus.setcity("Salt Lake City");
            bus.setzipCode(84663);

            String json = BusinessToJSON(bus);
            System.out.println(json);

            Business bus2 = JSONToBusiness(json);
            System.out.println(bus2);
    }
}
