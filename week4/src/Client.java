import java.util.List;
import java.util.Map;

public class Client {

    public static void main(String[] args) {
        System.out.println(HTTP.getHTTPContent("https://en.wikipedia.org"));

        Map<Integer, List<String>> m = HTTP.getHttpHeaders("https://en.wikipedia.org");

        for (Map.Entry<Integer, List<String>> entry : m.entrySet()) {
            System.out.println("Key = " + entry.getKey() + " Value = " + entry.getValue());
        }
    }
}