import java.net.*;
import java.io.*;
import java.util.*;

public class HTTP {

    public static String getHTTPContent(String string) {

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return "An error has occurred!";
    }

    public static Map getHttpHeaders(String string) {

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();
            return http.getHeaderFields();

        } catch (IOException e) {
            System.err.println(e.toString());
        }
        return null;
    }
}