public class Business {
    private String name;
    private long phone;
    private String address;
    private String city;
    private long zipCode;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public long getPhone() {
        return phone;
    }
    public void setPhone(long phone) {
        this.phone = phone;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getcity() {
        return city;
    }
    public void setcity(String city) {
        this.city = city;
    }
    public long getzipCode() {
        return zipCode;
    }
    public void setzipCode(long zipCode) {
        this.zipCode = zipCode;
    }
    public String toString() {
        return " Name: "  + name + " |" + " Phone: " + phone + " |" + " Address: " + address + " |" + " City: " + city + " |" + " Zip Code: " + zipCode;
    }
}
