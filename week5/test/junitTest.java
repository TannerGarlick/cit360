import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
public class junitTest {
    private final junit dog = new junit("Oscar");
    @Test
    public void testAssertEquals() {
        junit lastName = new junit("Garlick");
        assertEquals("Garlick", lastName.getName());
    }
    @Test
    public void testAssertFalse() {

        assertFalse(dog.isHappy());
    }
    @Test
    public void testAssertTrue() {
        dog.happyAfterWalk();
        assertTrue(dog.isHappy());
    }
    @Test
    public void testAssertSame() {
        int number = 1;
        int number1 = 1;
        assertSame(number, number1);
    }
    @Test
    public void testAssertNotSame() {
        Object cats = new Object();
        Object dogs = new Object();
        assertNotSame(cats, dogs);
    }
    int numberOfRocks = 123;
    @Test
    public void testAssertThat() {
        assertThat(numberOfRocks,is(123));
    }
    @Test
    public void testNotNullAndNull() {
        String string = null;
        String string1 = "Hi";
        assertNull(string);
        assertNotNull(string1);
    }
    @Test
    public void testArrayEquals() {
        String[] expectedOutput = {"red", "yellow", "blue"};
        String[] methodOutput = {"red", "yellow", "blue"};
        assertArrayEquals(expectedOutput, methodOutput);
    }
    @Test
    public void testEquals() {
        String num1 = "Hello";
        String num2 = "Hello";
        assertEquals(num1,num2);
    }
}