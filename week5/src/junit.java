public class junit {

    private String name;
    private boolean happy = false;

    public junit(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return happy;
    }

    public void happyAfterWalk() {
        happy = true;
    }

}
