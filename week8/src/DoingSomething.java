import java.util.Random;
public class DoingSomething implements Runnable {
private final String name;
private final int number;
private final int sleep;
private final int random;

    public DoingSomething(String name, int number, int sleep) {
        this.name = name;
        this.number = number;
        this.sleep = sleep;
        Random random = new Random();
        this.random = random.nextInt(1000);
        }

    public void run() {
        System.out.println("\nExecuting with these parameters: Name =" + name + " Number = "
                            + number + " Sleep = " + sleep + " Random Number = " + random + "\n");
        for (int count = 1; count < random; count++) {
            if (count % number == 0) {
                System.out.print(name + " is sleeping. ");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException | IllegalArgumentException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n" + name + " is done.\n");
    }
}