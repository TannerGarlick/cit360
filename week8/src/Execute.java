import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
public class Execute {

    public static void main(String[] args) {

        ExecutorService myService = Executors.newFixedThreadPool(3);
        DoingSomething ds1 = new DoingSomething("Bob", 25, 999);
        DoingSomething ds2 = new DoingSomething("Sally", 10, 444);
        DoingSomething ds3 = new DoingSomething("Billy", 5, 222);
        DoingSomething ds4 = new DoingSomething("Anna", 2, 111);
        DoingSomething ds5 = new DoingSomething("Pat", 1, 22);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
