import java.util.Scanner;
public class dataValidation {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter first number");
        while (scan.hasNextInt()) {
            int number1 = scan.nextInt();
            System.out.println("Enter second number");
            int number2 = scan.nextInt();
            if (number2 == 0){
                System.out.println("You cannot divide by 0! Please enter your first number.");
            }
            else {
                System.out.println("Result: " + number1 + " / " + number2 + " = " + number1 / number2);
            }
        }
        System.out.println("Please enter a number");
    }
}

