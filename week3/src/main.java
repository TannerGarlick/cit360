import java.util.*;
import java.util.Scanner;
public class main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        boolean error = false;
        do {
                try {
                    System.out.println("Enter first number");
                    int number1 = scan.nextInt();
                    System.out.println("Enter second number");
                    int number2 = scan.nextInt();
                    System.out.println("Result: " + number1 + " / " + number2 + " = " + number1 / number2);
                } catch (InputMismatchException e) {
                    System.out.println("Please enter a valid number");
                    error = true;
                } catch (ArithmeticException e) {
                    System.out.println("You should not divide a number by zero. Do not enter 0 as your second number.");
                    error = true;
                } catch (Exception e) {
                    System.out.println("Something else went wrong");
                    error = true;
                } finally {
                    System.out.println("Math is fun!");
                    scan.next();
                }
            }
        while(error);
    }
}