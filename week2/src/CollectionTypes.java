import java.util.*;

public class CollectionTypes {
    public static void main(String[] args) {
        System.out.println("~~~List~~~");
        ArrayList <String> list = new ArrayList <String>();
        list.add("Bread");
        list.add("Cheese");
        list.add("Milk");
        list.add("Eggs");
        list.add("Bread");
        Iterator iterate = list.iterator();
        while(iterate.hasNext()) {
            System.out.println(iterate.next());
        }
        System.out.println("~~~Set~~~");
        Set set = new TreeSet();
        set.add("Bread");
        set.add("Cheese");
        set.add("Milk");
        set.add("Eggs");
        set.add("Bread");
        for (Object str : set) {
            System.out.println((String)str);
        }
        System.out.println("~~~Queue~~~");
        Queue queue =  new PriorityQueue();
        queue.add("Bread");
        queue.add("Cheese");
        queue.add("Milk");
        queue.add("Eggs");
        queue.add("Bread");
        Iterator iterator = queue.iterator();
        while (iterator.hasNext()){
            System.out.println(queue.poll());
        }
        System.out.println("~~~Tree~~~");
        TreeSet treeSet = new TreeSet();
        treeSet.add("Bread");
        treeSet.add("Cheese");
        treeSet.add("Milk");
        treeSet.add("Eggs");
        treeSet.add("Bread");
        for (Object str : set) {
            System.out.println((String)str);
        }
        System.out.println("~~~Generics~~~");
        List <Books> myList = new ArrayList <Books>();
        myList.add(new Books("The Hunger Games","Suzanne Collins"));
        myList.add(new Books("Harry Potter and the Sorcerer's Stone","J.K. Rowling"));
        myList.add(new Books("The Hobbit","J.R.R. Tolkien"));
        myList.add(new Books("The Lion, the Witch and the Wardrobe","C.S. Lewis"));
        myList.add(new Books("The Giver","Lois Lowry"));
        for (Books book : myList){
            System.out.println(book);
        }
    }
}
